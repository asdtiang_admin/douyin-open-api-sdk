package vip.gadfly.tiktok.partner.bean.oauth2;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import vip.gadfly.tiktok.partner.bean.TpBaseResult;


@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class TpAuthAppAuthCodeResult extends TpBaseResult {

    @JSONField(name = "authorization_code")
    @JsonAlias("authorization_code")
    @JsonProperty("authorization_code")
    @SerializedName("authorization_code")
    private String authorizationCode;

    @JSONField(name = "expires_in")
    @JsonAlias("expires_in")
    @JsonProperty("expires_in")
    @SerializedName("expires_in")
    private Integer expiresIn;

}
