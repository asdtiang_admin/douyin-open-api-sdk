package vip.gadfly.tiktok.partner.bean.register;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import vip.gadfly.tiktok.partner.bean.TpDataResult;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class TpApplyResult extends TpDataResult<TpApplyResult.Result> {


    @NoArgsConstructor
    @Data
    public static class Result {
        /**
         * mobile
         * 手机号，若未注册抖音开放平台，则自动注册
         */
        @JsonProperty("apply_id")
        @JSONField(name = "apply_id")
        @JsonAlias("apply_id")
        @SerializedName("apply_id")
        private String applyId;
    }

}
