package vip.gadfly.tiktok.partner.bean.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ApplyMessage {


    /**
     * tpAppId
     */
    @JsonProperty("TpAppId")
    private String tpAppId;
    /**
     * eventTime
     */
    @JsonProperty("EventTime")
    private String eventTime;
    /**
     * event
     */
    @JsonProperty("Event")
    private String event;
    /**
     * applyId
     */
    @JsonProperty("ApplyId")
    private String applyId;
    /**
     * applyStatus
     */
    @JsonProperty("ApplyStatus")
    private String applyStatus;
    /**
     * verifyUrl
     */
    @JsonProperty("VerifyUrl")
    private String verifyUrl;
    /**
     * verifyStatus
     */
    @JsonProperty("VerifyStatus")
    private String verifyStatus;
    /**
     * failReason
     */
    @JsonProperty("FailReason")
    private String failReason;
}
