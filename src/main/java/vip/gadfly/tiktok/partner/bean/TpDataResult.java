package vip.gadfly.tiktok.partner.bean;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class TpDataResult<T> extends TpBaseResult {

    /**
     * 业务参数
     */
    @JSONField(name = "data")
    @JsonAlias("data")
    @JsonProperty("data")
    @SerializedName("data")
    private T data;

}
