package vip.gadfly.tiktok.partner.bean.register;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TpPreCheckRequest {

    /**
     * qualificationType
     * 主体类型
     * 1： 企业
     * 2：个体工商户
     * 3：党政机关
     * 4：事业单位
     * 5：港澳台
     * 6：社会组织
     * 7：境外
     * 100：其他组织
     */
    @JsonProperty("qualification_type")
    @JSONField(name = "qualification_type")
    @JsonAlias("qualification_type")
    @SerializedName("qualification_type")
    private Integer qualificationType;
    /**
     * companyName
     * 企业名称
     * ****有限责任公司
     */
    @JsonProperty("company_name")
    @JSONField(name = "company_name")
    @JsonAlias("company_name")
    @SerializedName("company_name")
    private String companyName;
    /**
     * creditCode
     * 营业执照编号
     */
    @JsonProperty("credit_code")
    @JSONField(name = "credit_code")
    @JsonAlias("credit_code")
    @SerializedName("credit_code")
    private String creditCode;
    /**
     * 营业执照有效期，格式：2006-01-02，永久用 0 表示
     */
    @JsonProperty("credit_end_date")
    @JSONField(name = "credit_end_date")
    @JsonAlias("credit_end_date")
    @SerializedName("credit_end_date")
    private String creditEndDate;
    /**
     * companyType
     * 企业类型
     * 1：有限责任公司
     * 2：有限责任公司（自然人独资）
     * 3：有限责任公司（自然人投资或控股）
     * 4：有限责任公司（法人独资）
     * 5：股份有限责任公司
     * 100：其他
     */
    @JsonProperty("company_type")
    @JSONField(name = "company_type")
    @JsonAlias("company_type")
    @SerializedName("company_type")
    private Integer companyType;
    /**
     * legalPersonName
     * 法人姓名
     */
    @JsonProperty("legal_person_name")
    @JSONField(name = "legal_person_name")
    @JsonAlias("legal_person_name")
    @SerializedName("legal_person_name")
    private String legalPersonName;
    /**
     * registeredProvinceCode
     * 注册地址，国家编号，参考国家及城市编号说明；主体类型为境外时必填，否则不填
     */
    @JsonProperty("registered_nation_code")
    @JSONField(name = "registered_nation_code")
    @JsonAlias("registered_nation_code")
    @SerializedName("registered_nation_code")
    private Integer registeredNationCode;
    /**
     * registeredProvinceCode
     * 注册地址，省份编号，参考国家及城市编号说明；主体类型不为境外时必填，否则不填
     */
    @JsonProperty("registered_province_code")
    @JSONField(name = "registered_province_code")
    @JsonAlias("registered_province_code")
    @SerializedName("registered_province_code")
    private Integer registeredProvinceCode;
    /**
     * registeredCityCode
     * 注册地址，城市编号，参考国家及城市编号说明；主体类型不为境外时必填，否则不填
     */
    @JsonProperty("registered_city_code")
    @JSONField(name = "registered_city_code")
    @JsonAlias("registered_city_code")
    @SerializedName("registered_city_code")
    private Integer registeredCityCode;
    /**
     * registeredAddress
     * 注册地址，详细地址
     * 海淀区*****
     */
    @JsonProperty("registered_address")
    @JSONField(name = "registered_address")
    @JsonAlias("registered_address")
    @SerializedName("registered_address")
    private String registeredAddress;
    /**
     * registeredCapital
     * 注册资本，单位元
     * 100000
     */
    @JsonProperty("registered_capital")
    @JSONField(name = "registered_capital")
    @JsonAlias("registered_capital")
    @SerializedName("registered_capital")
    private Integer registeredCapital;
}
