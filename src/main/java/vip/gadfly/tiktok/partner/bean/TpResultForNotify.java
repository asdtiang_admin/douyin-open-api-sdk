package vip.gadfly.tiktok.partner.bean;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TpResultForNotify {

    /**
     * 错误编号
     */
    @JSONField(name = "err_no")
    @JsonAlias("err_no")
    @JsonProperty("err_no")
    @SerializedName("err_no")
    private Integer errno;

    /**
     * 错误信息
     */
    @JSONField(name = "err_tips")
    @JsonAlias("err_tips")
    @JsonProperty("err_tips")
    @SerializedName("err_tips")
    private String errTips;

}
