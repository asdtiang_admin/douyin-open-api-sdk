package vip.gadfly.tiktok.partner.bean.register;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class TpApplyRequest extends TpPreCheckRequest {


    /**
     * mobile
     * 手机号，若未注册抖音开放平台，则自动注册
     */
    @JsonProperty("mobile")
    @JSONField(name = "mobile")
    @JsonAlias("mobile")
    @SerializedName("mobile")
    private String mobile;
    /**
     * creditImagePath
     * 营业执照照片路径
     * 需要使用第三方小程序应用上传资源接口返回的路径才可以，接口调用时入参 material_type 字段为 1，否则报错。
     */
    @JsonProperty("credit_image_path")
    @JSONField(name = "credit_image_path")
    @JsonAlias("credit_image_path")
    @SerializedName("credit_image_path")
    private String creditImagePath;
    /**
     * categoryCode
     * 经营行业类目编号，多个使用","隔开，参考经营行业类目编号说明
     */
    @JsonProperty("category_code")
    @JSONField(name = "category_code")
    @JsonAlias("category_code")
    @SerializedName("category_code")
    private String categoryCode;
    /**
     * businessPersonName
     * 运营人员姓名
     */
    @JsonProperty("business_person_name")
    @JSONField(name = "business_person_name")
    @JsonAlias("business_person_name")
    @SerializedName("business_person_name")
    private String businessPersonName;
    /**
     * businessIdCard
     * 运营人员身份证号
     */
    @JsonProperty("business_id_card")
    @JSONField(name = "business_id_card")
    @JsonAlias("business_id_card")
    @SerializedName("business_id_card")
    private String businessIdCard;
    /**
     * businessMobile
     * 运营人员手机号
     */
    @JsonProperty("business_mobile")
    @JSONField(name = "business_mobile")
    @JsonAlias("business_mobile")
    @SerializedName("business_mobile")
    private String businessMobile;
    /**
     * companyIntroduction
     * 公司简介，最多140个文字
     */
    @JsonProperty("company_introduction")
    @JSONField(name = "company_introduction")
    @JsonAlias("company_introduction")
    @SerializedName("company_introduction")
    private String companyIntroduction;
    /**
     * businessProvinceCode
     * 经营地址/住所，省份编号，参考城市编号说明
     */
    @JsonProperty("business_province_code")
    @JSONField(name = "business_province_code")
    @JsonAlias("business_province_code")
    @SerializedName("business_province_code")
    private Integer businessProvinceCode;
    /**
     * businessCityCode
     * 经营地址/住所，城市编号，参考城市编号说明
     */
    @JsonProperty("business_city_code")
    @JSONField(name = "business_city_code")
    @JsonAlias("business_city_code")
    @SerializedName("business_city_code")
    private Integer businessCityCode;
    /**
     * businessAddress
     * 经营地址/住所，详细地址
     */
    @JsonProperty("business_address")
    @JSONField(name = "business_address")
    @JsonAlias("business_address")
    @SerializedName("business_address")
    private String businessAddress;
    /**
     * employeeCount
     * 公司员工总数
     */
    @JsonProperty("employee_count")
    @JSONField(name = "employee_count")
    @JsonAlias("employee_count")
    @SerializedName("employee_count")
    private Integer employeeCount;
    /**
     * companyWebsite
     * 官网地址
     */
    @JsonProperty("company_website")
    @JSONField(name = "company_website")
    @JsonAlias("company_website")
    @SerializedName("company_website")
    private String companyWebsite;
}
