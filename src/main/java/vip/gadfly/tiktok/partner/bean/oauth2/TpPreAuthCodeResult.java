package vip.gadfly.tiktok.partner.bean.oauth2;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import vip.gadfly.tiktok.partner.bean.TpBaseResult;
import vip.gadfly.tiktok.partner.bean.TpDataResult;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class TpPreAuthCodeResult extends TpBaseResult {

    @JSONField(name = "pre_auth_code")
    @JsonAlias("pre_auth_code")
    @JsonProperty("pre_auth_code")
    @SerializedName("pre_auth_code")
    private String preAuthCode;

    @JSONField(name = "expires_in")
    @JsonAlias("expires_in")
    @JsonProperty("expires_in")
    @SerializedName("expires_in")
    private Integer expiresIn;

}
