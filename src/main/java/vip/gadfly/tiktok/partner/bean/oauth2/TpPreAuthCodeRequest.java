package vip.gadfly.tiktok.partner.bean.oauth2;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TpPreAuthCodeRequest {

    @JSONField(name = "pre_auth_code_type")
    @JsonAlias("pre_auth_code_type")
    @JsonProperty("pre_auth_code_type")
    @SerializedName("pre_auth_code_type")
    private Integer preAuthCodeType;

    @JSONField(name = "app_name")
    @JsonAlias("app_name")
    @JsonProperty("app_name")
    @SerializedName("app_name")
    private String appName;

    /**
     * /oauth/access_token/  下为  authorization_code
     * /oauth/refresh_token/  下为 refresh_token
     * /oauth/client_token/ 下为 client_credential
     */
    @JSONField(name = "app_icon")
    @JsonAlias("app_icon")
    @JsonProperty("app_icon")
    @SerializedName("app_icon")
    private String appIcon;

}
