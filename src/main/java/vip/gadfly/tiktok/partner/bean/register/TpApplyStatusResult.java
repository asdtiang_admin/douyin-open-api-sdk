package vip.gadfly.tiktok.partner.bean.register;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import vip.gadfly.tiktok.partner.bean.TpDataResult;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class TpApplyStatusResult extends TpDataResult<TpApplyStatusResult.Result> {


    @NoArgsConstructor
    @Data
    public static class Result {
        /**
         *
         * 入驻状态
         *
         * 1：帐号注册失败
         * 2：入驻中
         * 3：入驻失败
         * 4：入驻成功
         */
        @JsonProperty("apply_status")
        @JSONField(name = "apply_status")
        @JsonAlias("apply_status")
        @SerializedName("apply_status")
        private Integer applyStatus;

        /**
         * 对公认证链接，入驻成功后有值
         */
        @JsonProperty("verify_url")
        @JSONField(name = "verify_url")
        @JsonAlias("verify_url")
        @SerializedName("verify_url")
        private String verifyUrl;

        /**
         * 入驻失败原因
         */
        @JsonProperty("fail_reason")
        @JSONField(name = "fail_reason")
        @JsonAlias("fail_reason")
        @SerializedName("fail_reason")
        private String failReason;
    }

}
