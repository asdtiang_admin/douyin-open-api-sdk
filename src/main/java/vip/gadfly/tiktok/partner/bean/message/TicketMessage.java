package vip.gadfly.tiktok.partner.bean.message;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@Data
public class TicketMessage {


    /**
     * createTime
     */
    @JsonProperty("CreateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JSONField(name = "CreateTime")
    @JsonAlias("CreateTime")
    @SerializedName("CreateTime")
    private Date createTime;
    /**
     * event
     */
    @JsonProperty("Event")
    @JSONField(name = "Event")
    @JsonAlias("Event")
    @SerializedName("Event")
    private String event;
    /**
     * fromUserName
     */
    @JsonProperty("FromUserName")
    @JSONField(name = "FromUserName")
    @JsonAlias("FromUserName")
    @SerializedName("FromUserName")
    private String fromUserName;
    /**
     * msgType
     */
    @JsonProperty("MsgType")
    @JSONField(name = "MsgType")
    @JsonAlias("MsgType")
    @SerializedName("MsgType")
    private String msgType;
    /**
     * ticket
     */
    @JsonProperty("Ticket")
    @JSONField(name = "Ticket")
    @JsonAlias("Ticket")
    @SerializedName("Ticket")
    private String ticket;
}
