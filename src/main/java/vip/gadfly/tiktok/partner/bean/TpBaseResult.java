package vip.gadfly.tiktok.partner.bean;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TpBaseResult {

    /**
     * 企业号结果公共参数
     */
    @JSONField(name = "errno")
    @JsonAlias("errno")
    @JsonProperty("errno")
    @SerializedName("errno")
    private Integer errno;

    /**
     * 消息
     */
    @JSONField(name = "message")
    @JsonAlias("message")
    @JsonProperty("message")
    @SerializedName("message")
    private String message;

    /**
     * 请求的唯一 id
     */
    @JSONField(name = "log_id")
    @JsonAlias("log_id")
    @JsonProperty("log_id")
    @SerializedName("log_id")
    private String logId;

}
