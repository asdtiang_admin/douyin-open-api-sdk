package vip.gadfly.tiktok.partner.bean.material;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import vip.gadfly.tiktok.partner.bean.TpDataResult;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class TpMaterialResult extends TpDataResult<TpMaterialResult.Result> {



    @Data
    public static class Result {

        private String path;

    }

}
