package vip.gadfly.tiktok.partner.bean.oauth2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import vip.gadfly.tiktok.partner.bean.TpBaseResult;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class TpAuthAppAccessTokenResult extends TpBaseResult {


    /**
     * authorizerAccessToken
     */
    @JsonProperty("authorizer_access_token")
    private String authorizerAccessToken;
    /**
     * expiresIn
     */
    @JsonProperty("expires_in")
    private Integer expiresIn;
    /**
     * authorizerRefreshToken
     */
    @JsonProperty("authorizer_refresh_token")
    private String authorizerRefreshToken;
    /**
     * refreshExpiresIn
     */
    @JsonProperty("refresh_expires_in")
    private Integer refreshExpiresIn;
    /**
     * authorizerAppid
     */
    @JsonProperty("authorizer_appid")
    private String authorizerAppid;
    /**
     * authorizePermission
     */
    @JsonProperty("authorize_permission")
    private List<AuthorizePermissionBean> authorizePermission;

    /**
     * AuthorizePermissionBean
     */
    @NoArgsConstructor
    @Data
    public static class AuthorizePermissionBean {
        /**
         * id
         */
        @JsonProperty("id")
        private Integer id;
        /**
         * category
         */
        @JsonProperty("category")
        private String category;
        /**
         * description
         */
        @JsonProperty("description")
        private String description;
    }
}
