package vip.gadfly.tiktok.partner.bean.oauth2;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TpAccessTokenRequest {

    @JSONField(name = "component_appid")
    @JsonAlias("component_appid")
    @JsonProperty("component_appid")
    @SerializedName("component_appid")
    private String componentAppid;

    @JSONField(name = "component_appsecret")
    @JsonAlias("component_appsecret")
    @JsonProperty("component_appsecret")
    @SerializedName("component_appsecret")
    private String componentAppsecret;

    /**
     * /oauth/access_token/  下为  authorization_code
     * /oauth/refresh_token/  下为 refresh_token
     * /oauth/client_token/ 下为 client_credential
     */
    @JSONField(name = "component_ticket")
    @JsonAlias("component_ticket")
    @JsonProperty("component_ticket")
    @SerializedName("component_ticket")
    private String componentTicket;

}
