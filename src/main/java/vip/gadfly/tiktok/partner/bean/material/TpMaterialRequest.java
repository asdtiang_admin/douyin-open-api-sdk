package vip.gadfly.tiktok.partner.bean.material;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.File;

@Data
@Accessors(chain = true)
public class TpMaterialRequest {

    @JSONField(name = "material_type")
    @JsonAlias("material_type")
    @JsonProperty("material_type")
    @SerializedName("material_type")
    private Integer materialType;

    @JSONField(name = "material_file")
    @JsonAlias("material_file")
    @JsonProperty("material_file")
    @SerializedName("material_file")
    private File materialFile;

}
