package vip.gadfly.tiktok.partner.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import vip.gadfly.tiktok.config.TtOpHostConfig;
import vip.gadfly.tiktok.core.enums.TtOpApiUrl;


@AllArgsConstructor
@Getter
public enum TpApiUrl implements TtOpApiUrl {
    /**
     * 获取第三方小程序的 access_token
     */
    GET_COMPONET_ACCESS_TOKEN(TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v1/auth/tp/token?component_appid=%s&component_appsecret=%s&component_ticket=%s"),

    // 构造授权链接的时需要生成的预授权码
    GET_PRE_AUTH_CODE (TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v2/auth/pre_auth_code?component_appid=%s&component_access_token=%s"),

    // 构造给商家的授权链接
    GET_AUTHORIZATION_URL (TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/mappconsole/tp/authorization?component_appid=%s&pre_auth_code=%s&redirect_uri=%s"),

    // 直接获取授权链接
    GET_AUTHORIZATION_DIRECT_URL (TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v2/auth/gen_link?component_appid=%s&component_access_token=%s"),

    // 抖音小程序素材管理
    UPLOAD_PIC_MATERIAL(TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v1/tp/upload_pic_material?component_appid=%s&component_access_token=%s"),

    // 商家小程序 token 管理
    AUTH_APP_ACCESS_TOKEN(TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v1/oauth/token"),

    // 商家小程序 access_token 丢失的补偿机制
    AUTH_APP_RETRIEVE_AUTH_CODE(TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v1/auth/retrieve"),

    // 代入驻预审
    SETTLE_PRE_CHECK(TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v1/settle/pre_check?component_appid=%s&component_access_token=%s"),

    // 代入驻申请
    SETTLE_APPLY(TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v1/settle/apply?component_appid=%s&component_access_token=%s"),

    // 代入驻申请结果查询
    SETTLE_QUERY_APPLY(TtOpHostConfig.TIKTOK_PARTNER_OPEN_API_HOST_URL, "/openapi/v1/settle/get_apply_status?component_appid=%s&component_access_token=%s&apply_id=%s"),
    ;

    private final String prefix;
    private final String path;
}
